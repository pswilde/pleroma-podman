#!/bin/bash

source ./script_config.sh
touch ./config/prod.secret.exs

echo Stopping old $POD_NAME-web container...
podman stop $POD_NAME-web
podman rm $POD_NAME-web

podman run -d \
    --name $POD_NAME-web \
    --pod $POD_NAME-pod \
    -v ./uploads:/var/lib/pleroma/uploads:z \
    -v ./static:/var/lib/pleroma/static:z \
    -v ./config/prod.secret.exs:/var/lib/pleroma/config.exs \
    -e DB_USER=$PG_USER \
    -e DB_PASS=$PG_PASS \
    -e DB_NAME=$PG_NAME \
    -e DB_HOST=$PG_HOST \
    $PLEROMA_IMG


