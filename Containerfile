FROM elixir:1.14.1-alpine as build

ARG MIX_ENV=prod

WORKDIR /src

RUN apk update &&\
    apk add git gcc g++ musl-dev make cmake file-dev

RUN git clone https://git.pleroma.social/pleroma/pleroma.git /src
RUN git checkout develop

RUN	mix local.hex --force &&\
	mix local.rebar --force &&\
	mix deps.get --only prod &&\
	mkdir release &&\
	mix release --path release

LABEL maintainer="ops@pleroma.social" \
    org.opencontainers.image.title="pleroma" \
    org.opencontainers.image.description="Pleroma for Docker" \
    org.opencontainers.image.authors="ops@pleroma.social" \
    org.opencontainers.image.vendor="pleroma.social" \
    org.opencontainers.image.documentation="https://git.pleroma.social/pleroma/pleroma" \
    org.opencontainers.image.licenses="AGPL-3.0" \
    org.opencontainers.image.url="https://pleroma.social" \
    org.opencontainers.image.revision=$VCS_REF \
    org.opencontainers.image.created=$BUILD_DATE

FROM alpine:3.16

ARG BUILD_DATE
ARG VCS_REF

ARG HOME=/opt/pleroma
ARG DATA=/var/lib/pleroma

RUN apk update &&\
    apk add exiftool ffmpeg imagemagick libmagic ncurses postgresql-client

RUN addgroup pleroma &&\
    adduser --system --shell /bin/false -G pleroma --home ${HOME} pleroma &&\
    mkdir -p ${DATA}/uploads &&\
    mkdir -p ${DATA}/static &&\
    chown -R pleroma ${DATA} &&\
    mkdir -p /etc/pleroma &&\
    chown -R pleroma /etc/pleroma

USER pleroma

COPY --from=build --chown=pleroma:pleroma /src/release ${HOME}

COPY --from=build --chown=pleroma:pleroma /src/config/docker.exs /etc/pleroma/config.exs
COPY --from=build --chown=pleroma:pleroma /src/docker-entrypoint.sh ${HOME}


#RUN chown -R pleroma ${HOME} ${DATA}
#RUN chmod +x ${HOME}/docker-entrypoint.sh

EXPOSE 4000

WORKDIR /var/lib/pleroma 

ENTRYPOINT ["/opt/pleroma/docker-entrypoint.sh"]

