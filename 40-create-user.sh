#!/bin/bash

admin=""
echo "Username:"
read -r user
echo "Email:"
read -r email
echo "Make Admin? (y/N)"
read -r makeadmin
if [[ $makeadmin == "y" ]]; then
    admin="--admin"
fi
podman exec -it $POD_NAME-web /opt/pleroma/bin/pleroma_ctl user new $user $email $admin
echo Done.
