#!/bin/bash

DATE=$(date +"%Y-%m-%d")
PORT=4000
POD_NAME=pleroma
PG_USER=pleroma
PG_PASS=pleroma
PG_HOST=localhost
PG_NAME=pleroma
PLEROMA_VER=develop

PLEROMA_IMG="pleroma-$PLEROMA_VER-$DATE"
if [ -f ./.last ]; then
	PLEROMA_IMG=$(cat ./.last)
fi
echo $PLEROMA_IMG
