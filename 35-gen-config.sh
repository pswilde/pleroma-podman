#!/bin/bash

source ./script_config.sh

podman exec -it --user=0 --privileged $POD_NAME-web /opt/pleroma/bin/pleroma_ctl instance gen
podman exec $POD_NAME-web cat config/generated_config.exs > ./config/prod.secret.exs
echo .
echo ===============================
echo "Check files in config - if all looks good, restart your container"


