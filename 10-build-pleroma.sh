#!/bin/bash

source ./script_config.sh

podman build -f Containerfile --build-arg="BUILD_DATE=$DATE" --build-arg="VCS_REF=$PLEROMA_VER" -t $PLEROMA_IMG

ex=$?
if [[ $ex != 0 ]]; then
	echo $ex
	echo "Failed to compile"
	exit
fi
if [[ $ex == 0 ]]; then
	echo "Creating .last file..."
	if [ -f ./.last ]; then
		rm ./.last
	fi
	echo $PLEROMA_IMG > ./.last
fi
