#!/bin/bash

source ./script_config.sh
cp ./pleroma-pod.service.sample ./$POD_NAME.service

POD_STR=s/++POD_NAME++/$POD_NAME/
sed -i $POD_STR ./$POD_NAME.service
USER=$(whoami)
USR_STR=s/++USER++/$USER/
sed -i $USR_STR ./$POD_NAME.service

echo Done. Great! Now copy pleroma-pod.service to /etc/systemd/system/ and enable it!


